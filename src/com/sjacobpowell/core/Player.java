package com.sjacobpowell.core;

public class Player extends Entity {
	public boolean up = false;
	public boolean down = false;
	public boolean left = false;
	public boolean right = false;
	
	public Player(int color, int size) {
		sprite = new Bitmap(size, size);
		for(int i = 0; i < sprite.pixels.length; i++) {
			sprite.pixels[i] = color;
		}
		sprite = ArtTools.circlefy(sprite, size / 2.2);
	}
	
	public boolean isInSamePlace(Player other) {
		return up == other.up && down == other.down && left == other.left && right == other.right;
	}

}
