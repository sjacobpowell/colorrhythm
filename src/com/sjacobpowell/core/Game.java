package com.sjacobpowell.core;

import java.awt.event.KeyEvent;
import java.util.Random;

public class Game {
	public int time;
	private int width;
	private int height;
	private Random random;
	private int rhythmSpeed = 120;
	private int allowance = rhythmSpeed / 6;
	public Player player;
	public Player nextBeat;
	public Player currentBeat;
	public boolean hit = false;

	public Game(int width, int height) {
		this.width = width;
		this.height = height;
		random = new Random();
		player = new Player(0x73408F, 32);
		nextBeat = new Player(0x0000ff, 100);
		currentBeat = new Player(0xE2F48A, 64);
	}

	public void tick(boolean[] keys) {
		player.up = keys[KeyEvent.VK_UP];
		player.down = keys[KeyEvent.VK_DOWN];
		player.left = keys[KeyEvent.VK_LEFT];
		player.right = keys[KeyEvent.VK_RIGHT];
		move(player);
		if(isBeat()) {
			hit |= player.isInSamePlace(currentBeat);
		} else {
			hit = false;
		}
		if (isExactBeat()) {
			nextBeat.up = random.nextBoolean();
			nextBeat.down = random.nextBoolean();
			nextBeat.left = random.nextBoolean();
			nextBeat.right = random.nextBoolean();
			move(nextBeat);
		}
		if (isExactUpBeat()) {
			currentBeat.up = nextBeat.up;
			currentBeat.down = nextBeat.down;
			currentBeat.left = nextBeat.left;
			currentBeat.right = nextBeat.right;
			move(currentBeat);
		}
		time++;
	}

	public boolean isBeat() {
		int tick = time % rhythmSpeed;
		return Math.abs(rhythmSpeed - tick) < allowance || tick < allowance;
	}

	private boolean isExactUpBeat() {
		return time % rhythmSpeed == rhythmSpeed / 2;
	}

	private boolean isExactBeat() {
		return time % rhythmSpeed == 0;
	}

	public void move(Player object) {
		object.y = object.up ? nextBeat.sprite.height / 2 : object.up || object.down ? object.y : height / 2;
		object.y = object.down ? height - nextBeat.sprite.height / 2 : object.up || object.down ? object.y : height / 2;
		object.x = object.left ? nextBeat.sprite.width / 2 : object.left || object.right ? object.x : width / 2;
		object.x = object.right ? width - nextBeat.sprite.width / 2 : object.left || object.right ? object.x : width / 2;
	}

}
