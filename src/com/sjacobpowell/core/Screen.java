package com.sjacobpowell.core;

import java.util.stream.IntStream;

public class Screen extends Bitmap {

	public Screen(int width, int height) {
		super(width, height);
	}

	public void render(Game game) {
		IntStream.range(0, pixels.length).forEach(i -> pixels[i] = game.isBeat() ? (game.hit ? 0x00ff00 : 0xff0000) : 0x000000);
		drawBeat(game.nextBeat);
		if (game.isBeat()) {
			drawBeat(game.currentBeat);
		}
		drawBeat(game.player);
	}

	private void drawBeat(Player object) {
		draw(object.sprite, object.getXInt() - object.sprite.width / 2, object.getYInt() - object.sprite.height / 2);
	}
}
